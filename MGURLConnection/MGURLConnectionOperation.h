//
//  MGURLConnectionOperation.h
//  MGURLConnection
//
//  Created by Maciek Gierszewski on 05.08.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGURLConnection.h"

@interface MGURLConnectionOperation : NSOperation
- (instancetype)initWithRequest:(NSURLRequest *)request
			  sendProgressBlock:(MGURLConnectionProgressBlock)sendProgressBlock
		   receiveProgressBlock:(MGURLConnectionProgressBlock)receiveProgressBlock
					finishBlock:(MGURLConnectionCompletionBlock)finishBlock;

@property (nonatomic, assign) BOOL allowRedirects;
@end
