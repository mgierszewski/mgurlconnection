//
//  MGURLConnectionParameter.h
//  MGURLConnection
//
//  Created by Maciej Gierszewski on 24.07.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef MGURL_CONNECTION_PARAMETER_MACROS
#define MGURL_CONNECTION_PARAMETER_MACROS

#define MGURLConnectionParameterBody(paramValue) [MGURLConnectionParameter parameterWithName:nil value:(paramValue) type:MGURLConnectionParameterTypeBody]
#define MGURLConnectionParameterJSON(paramValue) [MGURLConnectionParameter parameterWithName:nil value:(paramValue) type:MGURLConnectionParameterTypeJSON]
#define MGURLConnectionParameterForm(name, paramValue) [MGURLConnectionParameter parameterWithName:(name) value:(paramValue) type:MGURLConnectionParameterTypeForm]
#define MGURLConnectionParameterHeader(name, paramValue) [MGURLConnectionParameter parameterWithName:(name) value:(paramValue) type:MGURLConnectionParameterTypeHeader]
#define MGURLConnectionParameterQuery(name, paramValue) [MGURLConnectionParameter parameterWithName:(name) value:(paramValue) type:MGURLConnectionParameterTypeQuery]

#endif

typedef NS_ENUM(NSInteger, MGURLConnectionParameterType)
{
    MGURLConnectionParameterTypeBody = 1,
    MGURLConnectionParameterTypeForm = 1<<1,
    MGURLConnectionParameterTypeJSON = 1<<2,
    MGURLConnectionParameterTypeHeader = 1<<3,
    MGURLConnectionParameterTypeQuery = 1<<4
};

@interface MGURLConnectionParameter : NSObject
@property (nonatomic, strong) id value;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) MGURLConnectionParameterType type;

@property (nonatomic, readonly) NSString *keyValueString;
@property (nonatomic, readonly) NSString *URLEncodedKeyValueString;

+ (instancetype)parameterWithName:(NSString *)name value:(id)value type:(MGURLConnectionParameterType)type;
@end
