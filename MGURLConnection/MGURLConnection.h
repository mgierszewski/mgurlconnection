//
//  MGURLConnection.h
//  MGURLConnection
//
//  Created by Maciej Gierszewski on 24.07.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MGURLConnectionParameter.h"
#import "NSString+MGURLConnection.h"

typedef void (^MGURLConnectionCompletionBlock)(NSHTTPURLResponse * _Nullable response, NSError * _Nullable error, id _Nullable data);
typedef void (^MGURLConnectionProgressBlock)(NSUInteger bytesLoaded, long long bytesTotal);

typedef NS_ENUM(NSInteger, MGURLConnectionHTTPMethod)
{
	MGURLConnectionHTTPMethodHead,
    MGURLConnectionHTTPMethodGet,
    MGURLConnectionHTTPMethodPost,
    MGURLConnectionHTTPMethodDelete,
    MGURLConnectionHTTPMethodPut
};

typedef NS_ENUM(NSInteger, MGURLConnectionErrorCode)
{
    MGURLConnectionErrorCodeAuthorizationRequired = 401,
};

@interface MGURLConnection : NSObject
@property (nullable, nonatomic, readonly) NSDictionary *headers;

@property (nonatomic, assign) BOOL allowRedirects;
@property (nonatomic, assign) NSTimeInterval timeoutInterval;

@property (nonatomic, assign) BOOL logRequests;
@property (nonatomic, assign) BOOL logResponses;
@property (nonatomic, assign) BOOL completionBlockOnMainThread;
@property (nonatomic, assign) CGFloat sentImagesQuality;

- (void)cancelAllRequests;

- (void)addHeaderValue:(NSString * _Nonnull)value forKey:(NSString * _Nonnull)key;
- (void)removeHeaderValueForKey:(NSString * _Nonnull)key;

- (void)sendRequestWithPath:(NSString * _Nonnull)path
                     params:(NSArray<MGURLConnectionParameter *> * _Nullable)params
                 HTTPMethod:(MGURLConnectionHTTPMethod)HTTPMethod
                 completion:(MGURLConnectionCompletionBlock _Nullable)completionBlock;

- (void)sendRequestWithPath:(NSString * _Nonnull)path
                     params:(NSArray<MGURLConnectionParameter *> * _Nullable)params
                 HTTPMethod:(MGURLConnectionHTTPMethod)HTTPMethod
               sendProgress:(MGURLConnectionProgressBlock _Nullable)progressBlock
            receiveProgress:(MGURLConnectionProgressBlock _Nullable)progressBlock
                 completion:(MGURLConnectionCompletionBlock _Nullable)completionBlock;

@end
