//
//  NSString+MGURLConnection.m
//  MGURLConnection
//
//  Created by Maciek Gierszewski on 07.11.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "NSString+MGURLConnection.h"

@implementation NSString (MGURLConnection)

- (NSString *)URLEncodedString
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(0,
                                                                                 (__bridge CFStringRef)self,
                                                                                 0,
                                                                                 CFSTR("=+[]{}$#@&"),
                                                                                 kCFStringEncodingUTF8));
}

@end
