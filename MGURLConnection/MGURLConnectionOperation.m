//
//  MGURLConnectionOperation.m
//  MGURLConnection
//
//  Created by Maciek Gierszewski on 05.08.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGURLConnectionOperation.h"

@interface MGURLConnectionOperation () <NSURLConnectionDataDelegate>
@property (nonatomic, strong) MGURLConnectionCompletionBlock finishBlock;
@property (nonatomic, strong) MGURLConnectionProgressBlock sendProgressBlock;
@property (nonatomic, strong) MGURLConnectionProgressBlock receiveProgressBlock;

@property (nonatomic, strong) NSURLRequest *originalRequest;

@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSHTTPURLResponse *response;

@property (nonatomic, strong) NSURLConnection *connection;

- (void)finish;
@end

@implementation MGURLConnectionOperation
{
	BOOL _executing;
	BOOL _finished;
}

- (instancetype)initWithRequest:(NSURLRequest *)request sendProgressBlock:(MGURLConnectionProgressBlock)sendProgressBlock receiveProgressBlock:(MGURLConnectionProgressBlock)receiveProgressBlock finishBlock:(MGURLConnectionCompletionBlock)finishBlock
{
	self = [super init];
	if (self)
	{
		_sendProgressBlock = sendProgressBlock;
		_receiveProgressBlock = receiveProgressBlock;
		_finishBlock = finishBlock;
		
		_data = nil;
		_error = nil;
		_response = nil;
		
		_executing = NO;
		_finished = NO;
		
		_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	}
	return self;
}

#pragma mark - Operation

- (void)start
{
//	if (![NSThread isMainThread])
//	{
//		dispatch_async(dispatch_get_main_queue(), ^{
//			[self start];
//		});
//		
//		return;
//	}
//	
	[self willChangeValueForKey:@"isExecuting"];
	_executing = YES;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self.connection setDelegateQueue:[NSOperationQueue currentQueue]];
	[self.connection start];
	[self main];
}

- (void)cancel
{
	[self.connection cancel];
	[super cancel];
}

- (void)finish
{
	if (self.finishBlock)
	{
		self.finishBlock(self.response, self.error, self.data);
	}
	
	[self willChangeValueForKey:@"isExecuting"];
	[self willChangeValueForKey:@"isFinished"];
	_executing = NO;
	_finished = YES;
	[self didChangeValueForKey:@"isExecuting"];
	[self didChangeValueForKey:@"isFinished"];
}

- (BOOL)isExecuting
{
	return _executing;
}

- (BOOL)isFinished
{
	return _finished;
}

- (BOOL)isAsynchronous
{
	return YES;
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	self.error = error;
	
	[self finish];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[self finish];
}

- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
	if (self.sendProgressBlock)
	{
		self.sendProgressBlock(totalBytesWritten, totalBytesExpectedToWrite);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
		self.response = (NSHTTPURLResponse *)response;
	}
	
	if (self.receiveProgressBlock)
	{
		self.receiveProgressBlock(0, response.expectedContentLength);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if (!self.data)
	{
		self.data = [NSMutableData new];
	}
	[self.data appendData:data];
	
	if (self.receiveProgressBlock)
	{
		self.receiveProgressBlock(self.data.length, self.response.expectedContentLength);
	}
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
	if (response != nil && !self.allowRedirects)
	{
		return nil;
	}
	
	NSMutableURLRequest *mutableRequest = [request mutableCopy];
	[connection.originalRequest.allHTTPHeaderFields enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
		if ([mutableRequest valueForHTTPHeaderField:key] == nil)
		{
			[mutableRequest addValue:obj forHTTPHeaderField:key];
		}
	}];
	
	return mutableRequest;
}

@end
