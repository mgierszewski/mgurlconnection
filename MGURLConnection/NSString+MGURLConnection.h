//
//  NSString+MGURLConnection.h
//  MGURLConnection
//
//  Created by Maciek Gierszewski on 07.11.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MGURLConnection)
- (NSString *)URLEncodedString;
@end
