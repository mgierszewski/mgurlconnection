//
//  MGURLConnection.m
//  MGURLConnection
//
//  Created by Maciej Gierszewski on 24.07.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGURLConnection.h"
#import "MGURLConnectionOperation.h"

@interface MGURLConnection () <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSMutableDictionary *mutableHeaders;

- (NSString *)HTTPMethodString:(MGURLConnectionHTTPMethod)HTTPMethod;
@end

@implementation MGURLConnection

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		_operationQueue = [NSOperationQueue new];
		
		_sentImagesQuality = 1.0f;
		_timeoutInterval = 30.0;
		_allowRedirects = YES;
		_completionBlockOnMainThread = YES;
		
		_mutableHeaders = [NSMutableDictionary new];
	}
	return self;
}

- (NSString *)HTTPMethodString:(MGURLConnectionHTTPMethod)HTTPMethod
{
	switch (HTTPMethod) {
		case MGURLConnectionHTTPMethodHead:
			return @"HEAD";
			break;
		case MGURLConnectionHTTPMethodGet:
			return @"GET";
			break;
		case MGURLConnectionHTTPMethodDelete:
			return @"DELETE";
			break;
		case MGURLConnectionHTTPMethodPut:
			return @"PUT";
			break;
		case MGURLConnectionHTTPMethodPost:
			return @"POST";
			break;
		default:
			break;
	}
	return nil;
}

#pragma mark - Setters

- (void)setSentImagesQuality:(CGFloat)sentImagesQuality
{
	_sentImagesQuality = MAX(0.0f, MIN(1.0f, sentImagesQuality));
}

#pragma mark - Headers

- (void)addHeaderValue:(NSString *)value forKey:(NSString *)key
{
	if (value && key)
	{
		[self.mutableHeaders setValue:value forKey:key];
	}
}

- (void)removeHeaderValueForKey:(NSString *)key
{
	if (key)
	{
		[self.mutableHeaders removeObjectForKey:key];
	}
}

- (NSDictionary *)headers
{
	return [NSDictionary dictionaryWithDictionary:self.mutableHeaders];
}

#pragma mark - Request

- (void)sendRequestWithPath:(NSString *)path
					 params:(NSArray<MGURLConnectionParameter *> *)params
				 HTTPMethod:(MGURLConnectionHTTPMethod)HTTPMethod
				 completion:(MGURLConnectionCompletionBlock)completionBlock;
{
	[self sendRequestWithPath:path params:params HTTPMethod:HTTPMethod sendProgress:nil receiveProgress:nil completion:completionBlock];
}

- (void)sendRequestWithPath:(NSString *)path
					 params:(NSArray<MGURLConnectionParameter *> *)params
				 HTTPMethod:(MGURLConnectionHTTPMethod)HTTPMethod
			   sendProgress:(MGURLConnectionProgressBlock)sendProgressBlock
			receiveProgress:(MGURLConnectionProgressBlock)receiveProgressBlock
				 completion:(MGURLConnectionCompletionBlock)completionBlock
{
#ifdef DEBUG
	NSParameterAssert(path != nil);
	NSParameterAssert(completionBlock != nil);
	NSParameterAssert(params == nil || [params isKindOfClass:[NSArray class]]);
#endif
	
	// get query params
	NSArray<MGURLConnectionParameter *> *queryParams = [params filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == %d", MGURLConnectionParameterTypeQuery]];
	if ([queryParams count] > 0)
	{
		if ([path rangeOfString:@"?"].location != NSNotFound)
		{
			path = [path stringByAppendingString:@"&"];
		}
		else
		{
			path = [path stringByAppendingString:@"?"];
		}
		
		NSArray *keyValueArray = [queryParams valueForKeyPath:NSStringFromSelector(@selector(URLEncodedKeyValueString))];
		path = [path stringByAppendingString:[keyValueArray componentsJoinedByString:@"&"]];
	}
	
	// create request
	NSURL* url = [NSURL URLWithString:path];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
														   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
													   timeoutInterval:self.timeoutInterval];
	
	[request setHTTPShouldHandleCookies:NO];
	[request setHTTPShouldUsePipelining:NO];
	[request setHTTPMethod:[self HTTPMethodString:HTTPMethod]];
	
	// "static" headers
	[self.mutableHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	// additional headers
	NSArray *headerParams = [params filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type == %d", MGURLConnectionParameterTypeHeader]];
	[headerParams enumerateObjectsUsingBlock:^(MGURLConnectionParameter *param, NSUInteger idx, BOOL *stop) {
		[request setValue:param.value forHTTPHeaderField:param.name];
	}];
	
	// attach other params
	NSArray *bodyParams = [params filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type & %d == type", MGURLConnectionParameterTypeBody]];
	NSArray *jsonParams = [params filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type & %d == type", MGURLConnectionParameterTypeJSON]];
	NSArray *formParams = [params filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type & %d == type", MGURLConnectionParameterTypeForm]];
	
	// there is single param and it's not a form param
	if ([formParams count] == 0 && [jsonParams count] + [bodyParams count] == 1)
	{
		if ([jsonParams count] == 1)
		{
			MGURLConnectionParameter *jsonParam = jsonParams.firstObject;
			if ([jsonParam.value isKindOfClass:[NSData class]])
			{
				[request setHTTPBody:jsonParam.value];
				[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
			}
			else
			{
				// RFC 7158 allows JSON document to consist entirely of any possible JSON typed value.
				NSString *body = [NSString stringWithFormat:@"%@", jsonParam.value];
				if ([jsonParam.value isKindOfClass:[NSString class]])
				{
					// wrap a string with " "
					if ([body characterAtIndex:0] != '\"')
					{
						body = [@"\"" stringByAppendingString:body];
					}
					if ([body characterAtIndex:(body.length - 1)] != '\"')
					{
						body = [body stringByAppendingString:@"\""];
					}
				}
				
				[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
				[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
			}
		}
		else if ([bodyParams count] == 1)
		{
			MGURLConnectionParameter *bodyParam = bodyParams.firstObject;
			NSString *body = [NSString stringWithFormat:@"%@", bodyParam.value];
			
			[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
			[request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
		}
	}
	else if ([formParams count] > 0 || [jsonParams count] > 1)
	{
		// there are multiple json params (more than one), or there are some form params
		
		// what if params contain image?
		NSArray *paramsWithImage = [formParams filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(MGURLConnectionParameter *parameter, NSDictionary *bindings) {
			return [parameter.value isKindOfClass:[UIImage class]];
		}]];
		
		if ([paramsWithImage count] + [jsonParams count] > 0)
		{
			// go with multipart/form-data content
			NSString *boundary = @"--------6f875f2289c9";
			
			NSMutableData *body = [NSMutableData new];
			for (MGURLConnectionParameter *parameter in [formParams arrayByAddingObjectsFromArray:jsonParams])
			{
				[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
				if ([parameter.value isKindOfClass:[UIImage class]])
				{
					// image parameter
					[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", parameter.name] dataUsingEncoding:NSUTF8StringEncoding]];
					[body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
					[body appendData:[NSData dataWithData:UIImageJPEGRepresentation(parameter.value, self.sentImagesQuality)]];
				}
				else if (parameter.type == MGURLConnectionParameterTypeForm)
				{
					// regular parameter
					[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameter.name] dataUsingEncoding:NSUTF8StringEncoding]];
					[body appendData:[[NSString stringWithFormat:@"%@", parameter.value] dataUsingEncoding:NSUTF8StringEncoding]];
				}
				else if (parameter.type == MGURLConnectionParameterTypeJSON && [parameter.value isKindOfClass:[NSData class]])
				{
					// json parameter
					[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", parameter.name] dataUsingEncoding:NSUTF8StringEncoding]];
					[body appendData:[[NSString stringWithFormat:@"Content-Type: application/json\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
					[body appendData:parameter.value];
				}
			}
			[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
			
			// set the content type
			[request setHTTPBody:body];
			[request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
		}
		else
		{
			// regular x-www-form-urlencoded content
			NSArray *paramsArray = [formParams valueForKeyPath:NSStringFromSelector(@selector(URLEncodedKeyValueString))];
			NSData *body = [[paramsArray componentsJoinedByString:@"&"] dataUsingEncoding:NSUTF8StringEncoding];
			
			// set the content type
			[request setHTTPBody:body];
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		}
	}
	
	//
	if (self.logRequests)
	{
		NSLog(@"\nurl: %@\nmethod: %@\nheaders: %@\nbody: %@\n", request.URL, request.HTTPMethod, request.allHTTPHeaderFields, [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
	}
	
	// operations
	MGURLConnectionOperation *connectionOperation = [[MGURLConnectionOperation alloc] initWithRequest:request sendProgressBlock:sendProgressBlock receiveProgressBlock:receiveProgressBlock finishBlock:^(NSHTTPURLResponse *response, NSError *error, NSData *data) {
		
		if (error && !response)
		{
			if (error.code == kCFURLErrorUserCancelledAuthentication && data.length > 0)
			{
				// unauthorized error -> proceed
				response = [[NSHTTPURLResponse alloc] initWithURL:request.URL
													   statusCode:MGURLConnectionErrorCodeAuthorizationRequired
													  HTTPVersion:@"1.1"
													 headerFields:nil];
			}
		}
		
		//		// remove escapes from data
		//		if (data.length > 0)
		//		{
		//			NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		//			dataString = [dataString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		//
		//			NSData *unescapedData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
		//			if (unescapedData)
		//			{
		//				data = unescapedData;
		//			}
		//		}
		
		id returnData = data;
		if (data.length > 0)
		{
			// check response content type
			__block NSString *responseContentType = nil;
			[response.allHeaderFields enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
				if ([[key lowercaseString] isEqual:@"content-type"])
				{
					responseContentType = obj;
					*stop = YES;
				}
			}];
			
			// parse the data
			if (responseContentType.length > 0 && [responseContentType rangeOfString:@"json"].location != NSNotFound)
			{
				// json data
				NSError *jsonError = nil;
				NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves|NSJSONReadingMutableContainers error:&jsonError];
				if (jsonDictionary)
				{
					returnData = jsonDictionary;
				}
				else if (jsonError)
				{
					error = jsonError;
				}
			}
			else if (responseContentType.length > 0 && [responseContentType rangeOfString:@"image"].location != NSNotFound)
			{
				// image data
				UIImage *image = [UIImage imageWithData:data];
				returnData = image;
			}
			else if (responseContentType.length > 0 && [responseContentType rangeOfString:@"text"].location != NSNotFound)
			{
				// plain data
				returnData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
			}
		}
		
		// log request
		if (self.logResponses)
		{
			NSLog(@"\ncode: %d\nheaders: %@\nbody: %@\n", (int)response.statusCode, response.allHeaderFields, returnData);
		}
		
		if (completionBlock)
		{
			if ([NSThread isMainThread] || !self.completionBlockOnMainThread)
			{
				completionBlock(response, error, returnData);
			}
			else
			{
				dispatch_async(dispatch_get_main_queue(), ^{
					completionBlock(response, error, returnData);
				});
			}
		}
	}];
	
	connectionOperation.allowRedirects = self.allowRedirects;
	[self.operationQueue addOperation:connectionOperation];
}

- (void)cancelAllRequests
{
	[self.operationQueue cancelAllOperations];
}

@end
