//
//  MGURLConnectionParameter.m
//  MGURLConnection
//
//  Created by Maciej Gierszewski on 24.07.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGURLConnectionParameter.h"
#import "NSString+MGURLConnection.h"
#import <UIKit/UIKit.h>

@interface MGURLConnectionParameter ()
@property (nonatomic, strong) id rawValue;
@end

@implementation MGURLConnectionParameter

+ (instancetype)parameterWithName:(NSString *)name value:(id)value type:(MGURLConnectionParameterType)type
{
    MGURLConnectionParameter *parameter = [MGURLConnectionParameter new];
	parameter.type = type;
	parameter.name = name;
    parameter.value = value;
	
    return parameter;
}

- (NSString *)keyValueString
{
	id name = self.name;
	id value = self.value;
	
	if ([value isKindOfClass:[NSArray class]])
	{
		NSMutableArray *keyValueComponents = [NSMutableArray array];
		[(NSArray *)value enumerateObjectsUsingBlock:^(id  _Nonnull subvalue, NSUInteger idx, BOOL * _Nonnull stop) {
			[keyValueComponents addObject:[NSString stringWithFormat:@"%@=%@", name, subvalue]];
		}];
				
		return [keyValueComponents componentsJoinedByString:@"&"];
	}
	return [NSString stringWithFormat:@"%@=%@", name, value];
}

- (NSString *)URLEncodedKeyValueString
{
	id name = self.name;
	id value = self.value;
	
	if ([value isKindOfClass:[NSString class]])
	{
		return [NSString stringWithFormat:@"%@=%@", name, [(NSString *)value URLEncodedString]];
	}
	else if ([value isKindOfClass:[NSArray class]])
	{
		NSMutableArray *keyValueComponents = [NSMutableArray array];
		[(NSArray *)value enumerateObjectsUsingBlock:^(id  _Nonnull subvalue, NSUInteger idx, BOOL * _Nonnull stop) {
			if ([subvalue isKindOfClass:[NSString class]])
			{
				[keyValueComponents addObject:[NSString stringWithFormat:@"%@=%@", name, [(NSString *)subvalue URLEncodedString]]];
			}
			else
			{
				[keyValueComponents addObject:[NSString stringWithFormat:@"%@=%@", name, subvalue]];
			}
		}];
		
		return [keyValueComponents componentsJoinedByString:@"&"];
	}
	return [NSString stringWithFormat:@"%@=%@", name, value];
}

- (void)setValue:(id)value
{
	if (self.type == MGURLConnectionParameterTypeJSON)
	{
		@try
		{
			self.rawValue = [NSJSONSerialization dataWithJSONObject:value options:NSJSONWritingPrettyPrinted error:nil];
		}
		@catch (NSException *exception)
		{
			self.rawValue = value;
		}
	}
	else
	{
		self.rawValue = value;
	}
}

- (id)value
{
	return self.rawValue;
}

@end
