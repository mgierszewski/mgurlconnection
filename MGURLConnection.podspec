Pod::Spec.new do |s|
	s.name			= "MGURLConnection"
	s.version		= "0.0.1"
	s.summary      = "NSURLConnection wrapper"
	s.author		= { "Maciej Gierszewski" => "m.gierszewski@futuremind.com" }
	s.platform		= :ios, '5.0'
	s.source		= { :git => "https://bitbucket.org/mgierszewski/mgurlconnection.git", :tag => "0.0.1" }
	s.source_files	= 'MGURLConnection'
	s.requires_arc	= true
	s.license		= { :type => 'WTFPL' }
	s.homepage		= 'https://bitbucket.org/mgierszewski/mgurlconnection.git'
end