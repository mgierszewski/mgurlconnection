//
//  MGURLConnectionTests.m
//  MGURLConnection
//
//  Created by Maciek Gierszewski on 05.08.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <MGURLConnection/MGURLConnection.h>

@interface MGURLConnectionTests : XCTestCase
@property (nonatomic, strong) MGURLConnection *connection;
@property (nonatomic, strong) MGURLConnection *secondConnection;
@end

@implementation MGURLConnectionTests

- (void)setUp
{
    [super setUp];
	
	_connection = [MGURLConnection new];
	_connection.logRequests = YES;
	_connection.logResponses = YES;
	
	_secondConnection = [MGURLConnection new];
	_secondConnection.logRequests = YES;
	_secondConnection.logResponses = YES;
	_secondConnection.completionBlockOnMainThread = NO;
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testConnection
{
	XCTestExpectation *expectation = [self expectationWithDescription:@"connection"];
	
	[self.connection sendRequestWithPath:@"http://pinky.futuremind.com/~dpaluch/test35/" params:nil HTTPMethod:MGURLConnectionHTTPMethodGet completion:^(NSHTTPURLResponse *response, NSError *error, id data) {
		[expectation fulfill];
	}];
	
	[self waitForExpectationsWithTimeout:self.connection.timeoutInterval handler:^(NSError *error) {
		XCTAssertNil(error);
	}];
}

- (void)testCompletionBlock
{
	XCTestExpectation *mainThreadExpectation = [self expectationWithDescription:@"mainThread"];
	XCTestExpectation *notMainThreadExpectation = [self expectationWithDescription:@"notMainThread"];
	
	[self.connection sendRequestWithPath:@"http://pinky.futuremind.com/~dpaluch/test35/" params:nil HTTPMethod:MGURLConnectionHTTPMethodGet completion:^(NSHTTPURLResponse *response, NSError *error, id data) {
		XCTAssertTrue([NSThread isMainThread]);
		[mainThreadExpectation fulfill];
	}];
	
	[self.secondConnection sendRequestWithPath:@"http://pinky.futuremind.com/~dpaluch/test35/" params:nil HTTPMethod:MGURLConnectionHTTPMethodGet completion:^(NSHTTPURLResponse *response, NSError *error, id data) {
		XCTAssertFalse([NSThread isMainThread]);
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[notMainThreadExpectation fulfill];
		});
	}];
	
	[self waitForExpectationsWithTimeout:self.connection.timeoutInterval handler:^(NSError *error) {
		XCTAssertNil(error);
	}];
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
