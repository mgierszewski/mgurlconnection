//
//  MGURLConnectionTests.m
//  MGURLConnectionTests
//
//  Created by Maciej Gierszewski on 24.07.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MGURLConnectionParameter.h"

@interface MGURLConnectionParameterTests : XCTestCase

@end

@implementation MGURLConnectionParameterTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testConstructors
{
    MGURLConnectionParameter *form = [MGURLConnectionParameter parameterWithName:@"form_name" value:@10 type:MGURLConnectionParameterTypeForm];
    MGURLConnectionParameter *query = [MGURLConnectionParameter parameterWithName:@"query_name" value:@10 type:MGURLConnectionParameterTypeQuery];
    MGURLConnectionParameter *header = [MGURLConnectionParameter parameterWithName:@"header_name" value:@10 type:MGURLConnectionParameterTypeHeader];
    MGURLConnectionParameter *body = [MGURLConnectionParameter parameterWithName:nil value:@10 type:MGURLConnectionParameterTypeBody];
    MGURLConnectionParameter *json = [MGURLConnectionParameter parameterWithName:nil value:@10 type:MGURLConnectionParameterTypeJSON];
    
    XCTAssertEqualObjects(form.value, @10, @"");
    XCTAssertEqualObjects(form.name, @"form_name", @"");
    XCTAssertEqual(form.type, MGURLConnectionParameterTypeForm, @"");
    
    XCTAssertEqualObjects(query.value, @10, @"");
    XCTAssertEqualObjects(query.name, @"query_name", @"");
    XCTAssertEqual(query.type, MGURLConnectionParameterTypeQuery, @"");
    
    XCTAssertEqualObjects(header.value, @10, @"");
    XCTAssertEqualObjects(header.name, @"header_name", @"");
    XCTAssertEqual(header.type, MGURLConnectionParameterTypeHeader, @"");
    
    XCTAssertEqualObjects(body.value, @10, @"");
    XCTAssertNil(body.name, @"");
    XCTAssertEqual(body.type, MGURLConnectionParameterTypeBody, @"");
    
    XCTAssertEqualObjects(json.value, @10, @"");
    XCTAssertNil(json.name, @"");
    XCTAssertEqual(json.type, MGURLConnectionParameterTypeJSON, @"");
}

- (void)testMacros
{
    MGURLConnectionParameter *form = MGURLConnectionParameterForm(@"form_name", @(10));
    MGURLConnectionParameter *query = MGURLConnectionParameterQuery(@"query_name", @(10));
    MGURLConnectionParameter *header = MGURLConnectionParameterHeader(@"header_name", @(10));
    MGURLConnectionParameter *body = MGURLConnectionParameterBody(@(10));
    MGURLConnectionParameter *json = MGURLConnectionParameterJSON(@(10));
    
    XCTAssertEqualObjects(form.value, @10, @"");
    XCTAssertEqualObjects(form.name, @"form_name", @"");
    XCTAssertEqual(form.type, MGURLConnectionParameterTypeForm, @"");
    
    XCTAssertEqualObjects(query.value, @10, @"");
    XCTAssertEqualObjects(query.name, @"query_name", @"");
    XCTAssertEqual(query.type, MGURLConnectionParameterTypeQuery, @"");
    
    XCTAssertEqualObjects(header.value, @10, @"");
    XCTAssertEqualObjects(header.name, @"header_name", @"");
    XCTAssertEqual(header.type, MGURLConnectionParameterTypeHeader, @"");
    
    XCTAssertEqualObjects(body.value, @10, @"");
    XCTAssertNil(body.name, @"");
    XCTAssertEqual(body.type, MGURLConnectionParameterTypeBody, @"");
    
    XCTAssertEqualObjects(json.value, @10, @"");
    XCTAssertNil(json.name, @"");
    XCTAssertEqual(json.type, MGURLConnectionParameterTypeJSON, @"");
}

- (void)testURLEncoding
{
	NSString *login = @"login+something@domain.com";
	MGURLConnectionParameter *param = MGURLConnectionParameterForm(@"login", login);
	NSString *keyValueString = param.URLEncodedKeyValueString;
	
	NSString *preparedKeyValueString = [NSString stringWithFormat:@"login=%@", login];
	XCTAssertNotEqualObjects(keyValueString, preparedKeyValueString);
	
	preparedKeyValueString = [preparedKeyValueString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
	preparedKeyValueString = [preparedKeyValueString stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
	XCTAssertEqualObjects(keyValueString, preparedKeyValueString);
}

- (void)testArrayParameters
{
	MGURLConnectionParameter *param = [MGURLConnectionParameter parameterWithName:@"name" value:@[@"value1", @"value2"] type:MGURLConnectionParameterTypeQuery];
	XCTAssertEqualObjects(param.keyValueString, @"name=value1&name=value2");
	
	MGURLConnectionParameter *param2 = [MGURLConnectionParameter parameterWithName:@"name" value:@[@"value&", @"value@"] type:MGURLConnectionParameterTypeQuery];
	XCTAssertEqualObjects(param2.URLEncodedKeyValueString, @"name=value%26&name=value%40");
}

@end
